import React, { Component } from "react";

const SENDER_NAME = "Dony";

export default class ChatRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chat: [
        {
          id: "123",
          senderName: "Lisa",
          message: "hello Dony",
          createdAt: new Date().getTime(),
        },
        {
          id: "123",
          senderName: "Dony",
          message: "hy juga",
          createdAt: new Date().getTime(),
        },
        {
          id: "123",
          senderName: "Lisa",
          message: "How Are you??",
          createdAt: new Date().getTime(),
        },
        {
          id: "123",
          senderName: "Dony",
          message: "iam fine",
          createdAt: new Date().getTime(),
        },
      ],
    };
  }

  typing = (e) => {
    const { chat } = this.state;
    if (e.keyCode === 13) {
      this.setState({
        chat: [
          ...chat,
          ...[{
            id: "123",
            senderName: SENDER_NAME,
            message: "p",
            createdAt: 123,
          }],
        ],
      });
    }
  };

  render() {
    const { chat } = this.state;
    return (
      <div className="h-100">
        <div className="chat-info">
          <div className="avatar" />
          <div className="name">bang jago</div>
        </div>
        <div className="chat-content">
          <div className="h-100 d-flex flex-column-reverse">
            {chat.reverse().map((item) => (
              <div
                className={`chat-baloon ${
                  item.senderName === SENDER_NAME && "text-right"
                } `}
              >
                <div
                  className={`chat-message ${
                    item.senderName === SENDER_NAME && "sender"
                  }`}
                >
                  {item.message}
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="chat-input">
          <input onKeyUp={this.typing} type="text" />
        </div>
      </div>
    );
  }
}
