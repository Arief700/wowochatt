import React, { Component } from "react";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";
import { Link } from "react-router-dom";
import firebase from "firebase";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      successMessage: "",
      buttonDisabled: true,
    };
  }
  Login = async (e) => {
    e.preventDefault();
    const { email, password } = this.state;
    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
    } catch (e) {
      console.log(e.code);
    }
    this.props.history.push("/ChatList");
  };

  changeText = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  formValidation = () => {
    const { email, password } = this.state;
    if (email !== "" && password !== "") {
      this.setState({ buttonDisabled: false });
    } else {
      this.setState({ buttonDisabled: true });
    }
  };
  componentDidMount() {
    if (this.props.location.state) {
      const { successMessage } = this.props.location.state;
      this.setState({ successMessage });
    }

    firebase.auth().onAuthStateChanged((e) => {
      this.props.history.push("/chatlist");
    });
  }
  render() {
    return (
      <div className="d-flex justify-content-center align-items-center h-100">
        <Form onSubmit={this.Login} className="form-login">
          <h3 className="text-center">WowoChatt Login</h3>
          {this.state.successMessage !== "" && (
            <h5 className="text-center success-message">
              {this.state.successMessage}
            </h5>
          )}
          <FormGroup>
            <Label for="email">Email</Label>
            <Input
              onChange={this.changeText}
              onKeyUp={this.formValidation}
              id="email"
              type="email"
              name="email"
            />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password</Label>
            <Input
              onChange={this.changeText}
              onKeyUp={this.formValidation}
              id="password"
              type="password"
              name="password"
            />
          </FormGroup>
          <Button disabled={this.state.buttonDisabled} block>
            Login
          </Button>

          <div className="mt-2">
            <span>
              Don't have an account? <Link to="/register">Register Here</Link>
            </span>
          </div>
        </Form>
      </div>
    );
  }
}
