import React, { Component } from "react";
import { Row, Col, Container } from "reactstrap";

import ChatRoom from './ChatRoom'

import firebase from '../helper/firebase'

export default class ChatList extends Component {
  render() {
    return (
      <div className="h-100 parent-chat ">
        <div className="accent-chat" />
        <Container className="container-chat h-100 ">
          <Row className="component-chat no-gutters">
            <Col md={4} className="chat-scroll">
              <h2>WowoChatt List</h2>
              <div className="chat-list">
                {[...Array(20)].map(() => (
                  <div className="chat-item">
                    <div className="chat-avatar" />
                    <div className="chat-content">
                      <span>Rambo</span>
                    </div>
                  </div>
                ))}
              </div>
            </Col>
            <Col md={8} className="chat-room">
              <ChatRoom/>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
