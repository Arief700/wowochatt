import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyCJdPUOIt2yffp9OjPQUG7weZpZ6ILWjnY",
  authDomain: "wowochatt.firebaseapp.com",
  projectId: "wowochatt",
  storageBucket: "wowochatt.appspot.com",
  messagingSenderId: "419836481330",
  appId: "1:419836481330:web:616a8e932cba81bcc9512b",
};

export default firebase.initializeApp(firebaseConfig);
